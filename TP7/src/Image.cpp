#include "Image.hpp"
#include <string>
#include <iostream>
#include <fstream>
#include <math.h>

Image::Image(int largeur, int hauteur) : _largeur(largeur), _hauteur(hauteur)
{
    _pixels=new int[_largeur*_hauteur];
    for(int k=0;k<_largeur*_hauteur;k++){

        _pixels[k]=0;

    }
}

Image::Image(const Image & img) : _largeur(img._largeur),_hauteur(img._hauteur){

    _pixels=new int[_largeur*_hauteur];

    for(int k=0;k<_largeur*_hauteur;k++){

        _pixels[k]=img._pixels[k];

    }

}

int Image::getLargeur()const{

    return _largeur;

}

int Image::getHauteur()const{

    return _hauteur;

}

int Image::getPixel(int i, int j) const{

    return _pixels[i*_largeur+j];
}

void Image::setPixel(int i, int j, int couleur){

    _pixels[i*_largeur+j]=couleur;

}

int & Image::pixel(int i, int j){   //modificateur

   return _pixels[i*_largeur+j];

}
const int & Image::pixel(int i, int j)const{ //acsesseur

   return _pixels[i*_largeur+j];
}

Image::~Image(){

    delete [] _pixels;

}

const Image & Image::operator=(const Image & img){
    if(&img !=this){
        _hauteur=img._hauteur;
        _largeur=img._largeur;
        delete [] _pixels;
        _pixels=new int[_largeur*_hauteur];

        for(int k=0;k<_largeur*_hauteur;k++){
            _pixels[k]=img._pixels[k];
        }
    }
    return *this;

}

void ecrirePnm(const Image & img, const std::string & nomFichier){

    std::ofstream ofs(nomFichier);
    ofs<<"P2"<<std::endl;
    ofs<<img.getLargeur()<<" "<<img.getHauteur()<<std::endl;
    ofs<<"255"<<std::endl;

    for(int i=0;i<img.getLargeur();i++){
        for(int j=0;j<img.getHauteur();j++){
            ofs<<img.pixel(i,j)<<" ";
        }
        ofs<<std::endl;
    }

}

void remplir(Image & img){

    double l= img.getLargeur();

    for(int j=0;j<img.getLargeur();j++){
        double t=2*M_PI*j/l;
        int c= (cos(t)+1)*127;
        for(int i=0;i<img.getHauteur();i++){
            img.pixel(i,j)=c;
        }
    }
}

Image bordure(const Image & img, int couleur){

    Image img2(img);
    const int il=img2.getLargeur()-1;
    const int jl=img2.getHauteur()-1;
    for(int i=0;i<img2.getHauteur();i++){
        img2.pixel(i,0)=couleur;
        img2.pixel(i,il)=couleur;
    }
    for (int j=0;j<img2.getLargeur();j++) {
        img2.pixel(0,j)=couleur;
        img2.pixel(jl,j)=couleur;
    }

return img2;
}
