#include "Image.hpp"

#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(img){};

TEST(img , test_const){

    Image i(10,10);
    CHECK_EQUAL(i.getLargeur(),10);
    CHECK_EQUAL(i.getHauteur(),10);

};

TEST(img, test_pix){

    Image i(10,10);
    i.setPixel(10,20,255);
    CHECK_EQUAL(255,i.getPixel(10,20));
};

TEST(img,test_pix2){

    Image i(10,10);
    i.pixel(10,20)=255;
    CHECK_EQUAL(255,i.pixel(10,20));

}
