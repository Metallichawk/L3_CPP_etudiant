#ifndef IMAGE_HPP
#define IMAGE_HPP

#include <string>
#include <iostream>

class Image
{
private:
    int _largeur;
    int _hauteur;
    int * _pixels;

public:
    Image(int largeur,int hauteur);
    Image(const Image & img);
    ~Image();
    int getLargeur()const;
    int getHauteur()const;
    int getPixel(int i, int j)const;
    void setPixel(int i, int j, int couleur);

    int & pixel(int i, int j);
    const int & pixel(int i, int j)const;

    const Image & operator=(const Image & img);

};

void ecrirePnm(const Image & img, const std::string & nomFichier);
void remplir(Image & img);
Image bordure(const Image & img, int couleur);

#endif // IMAGE_HPP
