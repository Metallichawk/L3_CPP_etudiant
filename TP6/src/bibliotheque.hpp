#ifndef BIBLIOTHEQUE_HPP
#define BIBLIOTHEQUE_HPP

#include <iostream>
#include <vector>
#include "livre.hpp"
#include <algorithm>
#include <fstream>

class Bibliotheque : public std::vector<Livre>
{
public:
    Bibliotheque();
    void afficher() const;
    void tierParAuteurEtTitre();
    void tierParAnnee();
    void ecrireFichier(const std::string & nomFicher)const;
};

#endif // BIBLIOTHEQUE_HPP
