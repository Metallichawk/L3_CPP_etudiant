#include "bibliotheque.hpp"

Bibliotheque::Bibliotheque()
{

}

void Bibliotheque::afficher() const{

    for(const Livre & l : *this){

        std::cout<<l<<std::endl;

    }

}

void Bibliotheque::tierParAuteurEtTitre(){

    sort(begin(),end());

}

void Bibliotheque::tierParAnnee(){
    auto cmp = [](const Livre& a,const Livre& b) {
        return a.getAnnee()<b.getAnnee();
    };
    sort(begin(),end(),cmp);

}

void Bibliotheque::ecrireFichier(const std::string & nomFicher)const{

    std::ofstream ofs(nomFicher);
    for(const Livre & l : *this){

        ofs <<l <<std::endl;

    }

}
