#ifndef ZONEDESSIN_HPP
#define ZONEDESSIN_HPP
#include <vector>
#include "figuregeometrique.hpp"
#include "polygoneregulier.hpp"
#include "couleur.hpp"
#include "point.hpp"
#include "ligne.hpp"
#include <gtkmm.h>

class ZoneDessin : Gtk::DrawingArea
{
private:
    std::vector<FigureGeometrique*> _figures;

public:
    ZoneDessin();
    ~ZoneDessin();

protected:
    bool on_draw(GdkEventExpose* event);
    bool gererClic(GdkEventButton* event);

};

#endif // ZONEDESSIN_HPP
