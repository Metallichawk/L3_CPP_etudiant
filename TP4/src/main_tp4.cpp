#include "figuregeometrique.hpp"
#include "ligne.hpp"
#include "polygoneregulier.hpp"
#include <vector>

int main(void){

    std::vector<FigureGeometrique*> figures {
        new Ligne({1,0,0},{0,0},{100,200}),
        new PolygoneRegulier({0,1,0},{100,200},50,5)

    };

    for(FigureGeometrique * f : figures){

        f->afficher();

    }

    for(FigureGeometrique * f : figures){

        delete f;

    }

    return 0;
}
