#include "ligne.hpp"

Ligne::Ligne(const Couleur & couleur, const Point & p0, const Point & p1):
    FigureGeometrique (couleur), _p0(p0), _p1(p1){}

void Ligne::afficher() const{

    //getCouleur()._r;
    Couleur coulTemp=getCouleur();
    Point p0=getP0();
    Point p1=getP1();

    std::cout<<"Ligne "<<coulTemp._r<<"_"<<coulTemp._g<<"_"<<coulTemp._b<<" "<<p0._x<<"_"<<p0._y
            <<" "<<p1._x<<"_"<<p1._y<<std::endl;

}

const Point & Ligne::getP0() const{

    return _p0;
}

const Point & Ligne::getP1() const{

    return _p1;
}
