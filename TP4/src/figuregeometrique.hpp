#ifndef FIGUREGEOMETRIQUE_HPP
#define FIGUREGEOMETRIQUE_HPP

#include <iostream>
#include "couleur.hpp"
#include "point.hpp"

class FigureGeometrique
{

//protected permet au classe qui hérite de FigureGeo d'acceder a ces paramètre sans que les autre classe le peuvent
protected:
    Couleur _couleur;

public:
    FigureGeometrique(const Couleur & couleur);
    virtual ~FigureGeometrique();
    virtual void afficher() const = 0;
    const Couleur & getCouleur() const;
};

#endif // FIGUREGEOMETRIQUE_HPP
