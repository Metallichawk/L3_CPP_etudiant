#include "figuregeometrique.hpp"
#include "ligne.hpp"

#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GrpLgn){ };

TEST(GrpLgn, test_getPoint){

    Point p;
    p._x=2;
    p._y=3;

    Couleur c;

    Ligne l(c,p,p);

    Point q=l.getP0();

    CHECK_EQUAL(p._x,q._x);
    CHECK_EQUAL(p._y,q._y);

};

TEST(GrpLgn, test_getCoul){

    Couleur c;

    c._b=1;
    c._r=1;
    c._g=1;

    Point p;

    Ligne l(c,p,p);

    Couleur temp=l.getCouleur();
    CHECK_EQUAL(temp._b,c._b);

};
